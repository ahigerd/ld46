Ludum Dare 46
-------------
**Theme:** Keep It Alive

**Team:** [Adam Higerd](https://ldjam.com/users/coda-highland) and [Ginny Higerd](https://ldjam.com/users/immamoonkin)

Lily vs. the Swarm
==================
It's spring, and that means new life! But not all of that life is friendly. Keep your friend, the butterfly, safe from the bad bugs!

Join this fairy and her friend in a child's imaginary adventure.

[Play on the web (HTML5)](http://greenmaw.com/ld46/)

Controls
--------
* Move: Arrow keys, WASD, or touchscreen D-pad
* Fire: Space bar or touchscreen button
* Bomb: Shift or touchscreen button

Defeated bugs drop energy. When your energy bar is full, a bomb becomes available. Bombs deal damage and heal you or the butterfly. You can carry up to 5 bombs at once.

The game is over if either you or the butterfly take too much damage. Watch out for the bosses!

![Screenshot](https://bitbucket.org/ahigerd/ld46/raw/master/preview-screenshot.png)

Too hard?
---------
This is a shoot-'em-up game, not quite a bullet hell or danmaku but still relatively challenging. If it's too difficult for you, the first time you get a game over you'll unlock an Easy Mode. In Easy Mode, you start with more bombs, you can survive more hits, and the butterfly is immune to damage so you only need to worry about protecting yourself.

Programmer's Notes
------------------
This is, without a doubt, hands down, the most content I have ever gotten into a jam game. It's also the most complete -- Nullified had game progression and a meaningful end state, but it was missing a lot of things that were in the plans. For this game, I think there was only one feature I wanted to get in that I didn't have time for, and one piece of artwork that Ginny drew that I didn't have time to clean up and incorporate.

It would have been nice to write another couple songs, but I'm INCREDIBLY happy to have an original composition of my own creation in here. The music in Nullified was my own composition as well, but I only did the remix of it during the jam. This was written in 3-4 hours Saturday night, and I haven't written any music in _years_.

License
=======
Software copyright (c) 2020 Adam Higerd

Sound and music copyright (c) 2020 Adam Higerd

Graphics copyright (c) 2020 Ginny and Adam Higerd

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

