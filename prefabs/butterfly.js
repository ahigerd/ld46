import Point from '../Point';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';
import CONFIG from '../config';

const tempVector = new Point(0, 0);

export default class Butterfly extends CharacterCore {
  constructor(origin) {
    super({
      label: 'player',
      animateHitboxes: false,
      defaultIsAnimating: true,
      defaultAnimationName: 'idle',
      hitbox: [-.4, -.2, .4, .2, 0x12],
      layer: 1,
      animations: {
        idle: [
          [assets.images.butterfly1, 0, 0, 64, 64, [.8, 1.2]],
          [assets.images.butterfly2, 0, 0, 64, 64, [.8, 1.2]],
          [assets.images.butterfly3, 0, 0, 64, 64, [.8, 1.2]],
          [assets.images.butterfly4, 0, 0, 64, 64, [.8, 1.2]],
          200.0,
        ],
        fast: [
          [assets.images.butterfly1, 0, 0, 64, 64, [.8, 1.2]],
          [assets.images.butterfly2, 0, 0, 64, 64, [.8, 1.2]],
          [assets.images.butterfly3, 0, 0, 64, 64, [.8, 1.2]],
          [assets.images.butterfly4, 0, 0, 64, 64, [.8, 1.2]],
          100.0,
        ],
        fall: [
          [assets.images.butterfly1, 0, 0, 64, 64, [.8, 1.2]],
          [assets.images.butterfly2, 0, 0, 64, 64, [.8, 1.2]],
          100.0,
        ],
      },
    }, origin);
    this.followMin = 2;
    this.followMax = 4;
    this.inertiaX = 0;
    this.inertiaY = 0;
    this.speed = 3.5 / 500;
    this.flutterTime = 0;
    this.flutter = 0;
  }

  start() {
    this.health = .1;
    this.maxHealth = 5;
  }

  update(scene, ms) {
    if (this.dead) {
      if (this.blinkTimer <= 0) {
        this.GameManager.gameOver();
      }
      this.blinkTimer -= ms * .5;
      assets.sounds.bgmLoop.volume = this.blinkTimer > 0 ? (this.blinkTimer / 2500) : 0;
      this.move((Math.random() - .5) * ms * .01, ms * .01);
      super.update(scene, ms);
      return;
    }

    const dx = GameManager.hero._origin[0] - this._origin[0] - 2;
    const dy = GameManager.hero._origin[1] - this._origin[1];
    const range = dx * dx + dy * dy;
    if (range > this.followMax) {
      this.inertiaX = this._calcInertia(dx, this.inertiaX);
      this.inertiaY = this._calcInertia(dy, this.inertiaY);
    } else if (range < this.followMin) {
      this.inertiaX = this._calcInertia(-dx, this.inertiaX) / 2;
      this.inertiaY = this._calcInertia(-dy, this.inertiaY) / 2;
    } else {
      if (Math.abs(this.inertiaX < 0.05)) {
        this.inertiaX = 0;
      } else {
        this.inertiaX *= 0.9;
      }
      if (Math.abs(this.inertiaY < 0.05)) {
        this.inertiaY = 0;
      } else {
        this.inertiaY *= 0.9;
      }
    }
    if (this.inertiaY > .5 && Math.abs(this.inertiaX) < .5) {
      this.setAnimation('fall');
      this.flutterTime = 0;
    } else if (Math.abs(this.inertiaX) + Math.abs(this.inertiaY) > .8) {
      this.flutterTime += 6.231852 / 400 * ms;
      if (this.currentAnimationName != 'fast') {
        this.setAnimation('fast');
        this.flutterTime = 0;
      }
    } else {
      this.flutterTime += 6.231852 / 800 * ms;
      if (this.currentAnimationName != 'idle') {
        this.setAnimation('idle');
        this.flutterTime = 0;
      }
    }
    this.flutter = Math.sin(this.flutterTime) * 0.1;
    this.move(this.inertiaX * this.speed * ms, this.inertiaY * this.speed * ms);

    tempVector.set(this._origin);
    tempVector.subtract(GameManager.hero._origin);
    if (tempVector.magnitude < 2) {
      tempVector.normalize();
      this.move(tempVector[0] * ms * .002, tempVector[1] * ms * .002);
    }

    if (this._origin[0] < GameManager.camera.aabb[0] + this.hitbox.width) {
      this._origin[0] = GameManager.camera.aabb[0] + this.hitbox.width;
    }

    super.update(scene, ms);
  }

  _calcInertia(delta, last) {
    const value = (Math.sign(delta) + last * 9) / 10;
    return (Math.abs(value) < 0.1) ? 0 : value;
  }

  render(camera) {
    this._origin[1] += this.flutter;
    super.render(camera);
    this._origin[1] -= this.flutter;
  }

  onCollisionEnter(other, coll) {
  }

  onCollisionStay(other, coll) {
  }

  onDamage() {
    if (CONFIG.easyMode) {
      this.health = this.maxHealth;
    } else {
      assets.sounds.butterflyHit.play();
    }
  }

  onDeath() {
    assets.sounds.gameOver.play();
  }
}

