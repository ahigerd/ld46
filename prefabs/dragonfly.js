import Point from '../Point';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';

const TAU = Math.PI * 2;

const prefab = {
  label: 'enemy',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'idle',
  hitbox: [-2.5, -1, 2.5, 1, 0x7],
  layer: 1,
  animations: {
    idle: [
      [assets.images.dragonfly, 0, 0, 160, 110, [2.5, 1.71875]],
    ],
  },
};

const tempVector = new Point(0, 0);
const delta = new Point(0, 0);

export default class Dragonfly extends CharacterCore {
  constructor(origin) {
    super(prefab, origin);
    this.lingerTime = 0;
  }

  start() {
    this.health = 4;
    this.maxHealth = 4;
    this.speed = (5.0 + GameManager.difficulty) / 300;
    this.velocity = new Point(-1, 0);
    this.shotCooldown = 0;
    this.ammo = this.lingerTime ? 0 : 3;
    this.target = new Point(Math.random() * 11, Math.random() * 16 - 8);
    this.fireArc = 0;
    this.hoverTime = 0;
  }

  update(scene, ms) {
    let mag = this.speed * ms;
    if (this.dead) {
      if (!this.didDrop) {
        this.didDrop = true;
        scene.add(new assets.prefabs.energy(this.origin, GameManager.hero));
      }
      this.move((Math.random() - .5) * mag * .5, mag);
      super.update(scene, ms);
      return;
    }

    if (this.lingerTime > 0) {
      this.lingerTime -= ms;
      this.hoverTime += ms;
      if (this.shotCooldown < 0 && this.ammo > 0) {
        this.shotCooldown = 270 - GameManager.difficulty * 30;
        scene.add(new assets.prefabs.bullet([this._origin[0] - 2, this._origin[1]], -.02, this.fireArc + Math.random() * .004 - .002, 'dragonfire'));
        this.ammo--;
      } else {
        this.shotCooldown -= ms;
      }
      this.move(0, ms * Math.sin(this.hoverTime * .02) * .001);
    } else {
      delta.set(this.target);
      delta.subtract(this._origin);
      if (delta.magnitude < 1) {
        let ok = false;
        let retry = 100;
        const rects = [];
        for (const other of scene.objects) {
          if (other.layer == this.layer) {
            rects.push(other.hitbox.translated(other._origin));
          }
        }
        do {
          this.target.setXY(Math.random() * 11, Math.random() * 16 - 8);
          ok = true;
          for (const other of rects) {
            if (other.contains(this.target)) {
              --retry;
              ok = false;
              break;
            }
          }
        } while (!ok && retry > 0);
        this.lingerTime = Math.random() * 500 + 1200 - 100 * GameManager.difficulty;
        this.shotCooldown = 0;
        this.hoverTime = 0;
        this.ammo = 3;
        this.fireArc = Math.random() * .02 - .01;
      } else {
        delta.clampMagnitude(1);
        for (const other of scene.objects) {
          if (other == this || other.label != 'enemy') continue;
          tempVector.set(this._origin);
          tempVector.subtract(other._origin);
          if (tempVector.magnitudeSquared < 4) {
            tempVector.normalize();
            delta.add(tempVector);
          }
        }
        delta.clampMagnitude(1);
        this.move(delta[0] * mag, delta[1] * mag);
      }
    }

    super.update(scene, ms);
  }

  onCollisionEnter(other, coll) {
    if (other.label == 'player') {
      other.inflict(1);
    }
  }

  onDeath() {
    if (!this.selfDestructed) {
      assets.sounds.mosquitoHit.play();
      GameManager.addScore(50);
    }
    this.stateTime = 0;
    if (!this.spawnedByBoss) {
      GameManager.enemyDown();
    }
  }
}
