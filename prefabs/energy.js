import Point from '../Point';
import Sprite from '../Sprite';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';

const tempVector = new Point(0, 0);
const prefab = {
  label: 'bullet',
  animateHitboxes: true,
  defaultIsAnimating: true,
  defaultAnimationName: 'default',
  hitbox: [-.01, -.01, .01, .01, 0],
  layer: 1,
  animations: {
    default: [
      [assets.images.energy, 0, 0, 20, 16, [.3125, .25]],
    ],
    big: [
      [assets.images.energy1, 0, 0, 32, 32, [22/32, 19/32]],
      [assets.images.energy2, 0, 0, 32, 32, [13/32, 22/32]],
      [assets.images.energy1, 0, 0, -32, -32, [10/32, 13/32]],
      [assets.images.energy2, 0, 0, -32, -32, [19/32, 10/32]],
      33,
    ],
  },
};

export default class EnergyPellet extends Sprite {
  constructor(origin, target, visual = 'default') {
    super(prefab, origin);
    this.target = target;
    this.setAnimation(visual);
    this.age = 0;
  }

  update(scene, ms) {
    if (this.age < 500) this.age += ms * .5;
    tempVector.set(this.target.origin);
    tempVector.subtract(this._origin);
    const mag = tempVector.magnitude;
    if (mag < 0.25) {
      if (this.target.maxEnergy) {
        if (this.target.energy < this.target.maxEnergy) {
          this.target.energy++;
        }
      } else if (this.target.health < this.target.maxHealth) {
        this.target.health++;
        assets.sounds.heal.play();
      }
      scene.remove(this);
      return;
    }
    let speed = ms * (this.currentAnimationName == 'big' ? .25 : 1) * this.age * .0002;
    if (speed > mag) speed = mag;
    tempVector.normalize();
    this.move(tempVector[0] * speed, tempVector[1] * speed, true);
  }
}

