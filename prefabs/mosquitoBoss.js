import Point from '../Point';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';

const prefab = {
  label: 'enemy',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'idle',
  hitbox: [-5, -4, 5, 2, 0x7],
  layer: 1,
  animations: {
    idle: [
      [assets.images.mosquitoBoss, 0, 0, 640, 395, [10, 395/64]],
    ],
  },
};

const tempVector = new Point(0, 0);

export default class MosquitoBoss extends CharacterCore {
  constructor(origin) {
    super(prefab, origin);
    this.state = 'enter';
    this.stateTime = Math.random() * 2;
    this.speed = 3.0 / 500;
    this.fireRate = 250;
    this.maxHealth = 25;
  }

  start() {
    this.health = 25;
    this.speed = (5.0 + GameManager.difficulty) / 700;
    this.spawnCooldown = this.fireRate;
  }

  update(scene, ms) {
    if (this.state == 'merge') return; // handled by monstrosity
    this.stateTime += ms * .01;
    let mag = this.speed * ms;
    let swarmX = Math.sin(this.stateTime * 3) * mag - Math.cos(this.stateTime * .7) * mag * .5;
    if (this.dead) {
      if (!this.didDrop) {
        this.didDrop = true;
        for (let i = 0; i < GameManager.hero.maxEnergy; i++) {
          tempVector[0] = this.lastAabb[0] + this.lastAabb.width * Math.random();
          tempVector[1] = this.lastAabb[1] + this.lastAabb.height * Math.random();
          scene.add(new assets.prefabs.energy(tempVector, GameManager.hero));
        }
      }
      this.stateTime += ms * .01;
      this.move(swarmX * this.stateTime, mag);
      super.update(scene, ms);
      return;
    }
    let swarmY = Math.sin(this.stateTime * 2.01) * mag * .25 + Math.sin(this.stateTime * .7) * mag;
    if (this._origin[1] < -5) {
      swarmY = .2 * mag;
    } else if (this._origin[1] > 5) {
      swarmY = -.2 * mag;
    }
    if (this.state == 'enter') {
      this.move(swarmX - mag * .8, swarmY);
      if (this._origin[0] < 10.5 && this._origin[1] > -5 && this._origin[1] < 5) {
        this.state = 'swarm';
        this.stateTime = Math.random() * 2;
      }
    } else {
      if (this._origin[0] > 13) {
        swarmX = -mag;
      } else if (this._origin[0] > 8) {
        swarmX -= .2 * mag;
      } else if (this._origin[0] < -12) {
        swarmX += .3 * mag
      }
      this.move(swarmX, swarmY);

      this.spawnCooldown -= ms;
      if (this.spawnCooldown < 0) {
        this.spawnCooldown = this.fireRate;
        const angle = (Math.random() + .5) * Math.PI;
        tempVector[0] = this._origin[0] + Math.cos(angle) * 1;
        tempVector[1] = this._origin[1] + Math.sin(angle) * 1;
        const mosquito = new assets.prefabs.mosquito(tempVector);
        mosquito.spawnedByBoss = true;
        mosquito.state = 'swarm';
        mosquito.swarmTime = .5;
        mosquito.swarmTransition = 1;
        scene.add(mosquito);
      }
    }
    super.update(scene, ms);
  }

  onCollisionEnter(other, coll) {
    if (other.label == 'player') {
      // why are you getting in melee range with the boss?
      other.inflict(2);
    }
  }

  onDamage() {
    assets.sounds.bossHit.play();
  }

  onDeath() {
    assets.sounds.bossDead.play();
    this.stateTime = 0;
    GameManager.addScore(1000);
    GameManager.enemyDown();
    for (const other of GameManager.scene.objects) {
      if (other instanceof assets.prefabs.mosquito) {
        other.selfDestruct();
      }
    }
  }
}
