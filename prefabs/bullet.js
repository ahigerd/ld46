import Sprite from '../Sprite';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';

const prefab = {
  label: 'bullet',
  animateHitboxes: true,
  defaultIsAnimating: true,
  defaultAnimationName: 'default',
  hitbox: [-.2, -.3, .4, .3, 0x4],
  layer: 1,
  animations: {
    default: [
      [assets.images.fireball, 0, 0, 32, 24, [.5, .375], [-.2, -.3, .4, .3, 0xC]],
    ],
    flower1: [
      [assets.images.flower1a, 0, 0, 32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      [assets.images.flower1b, 0, 0, 32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      [assets.images.flower1a, 0, 0, -32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      [assets.images.flower1b, 0, 0, -32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      33.
    ],
    flower2: [
      [assets.images.flower2a, 0, 0, 32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      [assets.images.flower2b, 0, 0, 32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      [assets.images.flower2a, 0, 0, -32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      [assets.images.flower2b, 0, 0, -32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      33.
    ],
    flower3: [
      [assets.images.flower3a, 0, 0, 32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      [assets.images.flower3b, 0, 0, 32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      [assets.images.flower3a, 0, 0, -32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      [assets.images.flower3b, 0, 0, -32, 32, [.5, .5], [-.5, -.5, .5, .5, 0xC]],
      33.
    ],
    stinger: [
      [assets.images.stinger, 0, 0, 40, 16, [0.625, .25], [-.25, -.2, .25, .2, 0x18]],
    ],
    dragonfire: [
      [assets.images.dragonfire, 0, 0, 59, 16, [59/64, .25], [-.25, -.2, .25, .2, 0x18]],
    ],
  },
};

const damageByType = {
  flower1: 3,
  flower2: 3,
  flower3: 3,
};

const healthByType = {
  flower1: 2,
  flower2: 3,
  flower3: 4,
};

export default class Bullet extends Sprite {
  constructor(origin, dx, dy, type = 'default') {
    super(prefab, origin);
    this.dx = dx;
    this.dy = dy;
    this.damage = damageByType[type] || 1;
    this.type = type;
    this.isBomb = type.includes('flower');
  }

  start() {
    this.setAnimation(this.type);
    this.health = healthByType[this.type] || 1;
    this.age = 0;
  }

  update(scene, ms) {
    this.age += ms * .001;
    if (this.isBomb) {
      const t = this.age * 4;
      const exp = Math.exp(t * .5 + this.dy);
      this._origin[0] = .1 * exp * Math.cos(t + this.dx) + GameManager.hero._origin[0];
      this._origin[1] = .1 * exp * Math.sin(t + this.dx) + GameManager.hero._origin[1];
    } else {
      this.move(this.dx * ms, this.dy * ms, true);
    }
  }

  lateUpdate(scene) {
    if (this.isBomb) {
      if (this._origin[0] < -25 || this._origin[0] > 15 || this._origin[1] < -15 || this._origin[1] > 15) {
        scene.remove(this);
      }
    } else if (!GameManager.camera.aabb.intersects(this.lastAabb)) {
      scene.remove(this);
    }
  }

  onCollisionEnter(other, coll) {
    if (this.isBomb && this.scene && other.label == 'bullet') {
      this.scene.remove(other);
    } else if (other.label == 'bullet' || !this.scene) {
      return;
    } else {
      other.inflict(this.damage);
    }
    --this.health;
    if (this.health <= 0) {
      this.scene.remove(this);
    }
  }
}
