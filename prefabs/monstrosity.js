import Point from '../Point';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';

const prefab = {
  label: 'enemy',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'idle',
  hitbox: [-12, -3.5, 9, 3, 0x7],
  layer: 1,
  animations: {
    idle: [
      [assets.images.monstrosity, 0, 0, 720, 492, [16, 492/64]],
    ],
  },
};

const tempVector = new Point(0, 0);

export default class Monstrosity extends CharacterCore {
  constructor(origin) {
    super(prefab, origin);
    this.state = 'enter';
  }

  start() {
    this.health = .1;
    this.maxHealth = 40;
    this.speed = (5.0 + GameManager.difficulty) / 700;
    this.stateTime = 0;
    this.hidden = true;
    this.immune = true;
    this.flashFrame = 0;
    this.spawnRate = 1000;
    this.spawnCooldown = 500;
    this.fireRate = 1000;
    this.fireCooldown = 1500;
    this.hasCollision = false;
  }

  update(scene, ms) {
    this.stateTime += ms * .01;

    if (this.state == 'enter') {
      this.hasCollision = false;
      this.mosquito.health = this.mosquito.maxHealth = 1;
      this.wasp.health = this.wasp.maxHealth = 1;
      this.dragonfly.health = this.dragonfly.maxHealth = 1;
      let ready = 0;
      if (this.mosquito._origin[1] < 0) {
        this.mosquito.move(0, ms * .005);
      } else {
        ready++;
      }
      if (this.wasp._origin[1] > 0) {
        this.wasp.move(0, ms * -.005);
      } else {
        ready++;
      }
      if (this.dragonfly._origin[0] > 8) {
        this.dragonfly.move(ms * -.005, 0);
      } else {
        ready++;
      }
      if (ready == 3) {
        this.state = 'merge';
        this.stateTime = 0;
      }
      return;
    } else if (this.state == 'merge') {
      this.forceShowHealth = true;
      if (this.health > this.maxHealth / 2) {
        this.flashFrame = (this.flashFrame + 1) % 6;
      } else {
        this.flashFrame = (this.flashFrame + 1) % 3;
      }
      this.mosquito.hidden = this.flashFrame != 0;
      this.wasp.hidden = this.flashFrame != 1;
      this.dragonfly.hidden = this.flashFrame != 2;
      this.hidden = this.flashFrame != 5;
      this.health += ms * .0004 * this.maxHealth;
      if (this.health >= this.maxHealth) {
        this.health = this.maxHealth;
        this.state = 'up';
        this.mosquito.hidden = true;
        this.mosquito.selfDestruct();
        scene.remove(this.mosquito);
        this.wasp.hidden = true;
        this.wasp.selfDestruct();
        scene.remove(this.wasp);
        this.dragonfly.hidden = true;
        this.dragonfly.selfDestruct();
        scene.remove(this.dragonfly);
        this.hidden = false;
        this.immune = false;
        this.hasCollision = true;
      }
      return;
    }


    let mag = this.speed * ms;
    if (this.dead) {
      if (this._origin[1] < 10) {
        tempVector[0] = this.lastAabb[0] + this.lastAabb.width * Math.random();
        tempVector[1] = this.lastAabb[1] + this.lastAabb.height * Math.random();
        scene.add(new assets.prefabs.energy(tempVector, GameManager.hero));
      }
      this.stateTime += ms * .01;
      const swarmX = Math.sin(this.stateTime * 3) * mag - Math.cos(this.stateTime * .7) * mag * .5;
      this.move(swarmX * this.stateTime, ms * .01 * Math.random());
      if (this._origin[1] > 15) {
        scene.remove(this);
        GameManager.newWave();
        GameManager.hero.win();
      } else {
        super.update(scene, ms);
      }
      return;
    }

    this.spawnCooldown -= ms;
    if (this.spawnCooldown < 0) {
      this.spawnCooldown += this.spawnRate;
      const angle = (Math.random() + .5) * Math.PI;
      tempVector[0] = this._origin[0] + Math.cos(angle) * 1;
      tempVector[1] = this._origin[1] + Math.sin(angle) * 1;
      const mosquito = new assets.prefabs.mosquito(tempVector);
      mosquito.spawnedByBoss = true;
      mosquito.state = 'swarm';
      mosquito.swarmTime = .5;
      mosquito.swarmTransition = 1;
      scene.add(mosquito);
    }

    this.fireCooldown -= ms;
    if (this.fireCooldown < 0) {
      this.fireCooldown += this.fireRate;
      tempVector[0] = this._origin[0] + this.hitbox[0];
      tempVector[1] = this._origin[1] + this.hitbox[1] + (.25 + Math.random() * .5) * this.hitbox.height;
      scene.add(new assets.prefabs.bullet(tempVector, -.04, 0, 'stinger'));
    }


    let dx = 0, dy = 0;
    if (this.state == 'enter') {
      this.move(-mag * .8, Math.sin(this.stateTime) * mag * .2);
      if (this._origin[0] < 12) {
        this.state = Math.random() > .5 ? 'up' : 'down';
      }
    } else {
      if (this.state == 'up') {
        this.move(0, -mag);
        if (this._origin[1] < -7) {
          this.state = 'fireUp';
          this.stateTime = 0;
          const dragonfly = new assets.prefabs.dragonfly([20, Math.random() * -10]);
          dragonfly.spawnedByBoss = true;
          scene.add(dragonfly);
        }
      } else if (this.state == 'down') {
        this.move(0, mag);
        if (this._origin[1] > 7) {
          this.state = 'fireDown';
          this.stateTime = 0;
          const dragonfly = new assets.prefabs.dragonfly([20, Math.random() * 10]);
          dragonfly.spawnedByBoss = true;
          scene.add(dragonfly);
        }
      } else {
        this.move(0, Math.sin(this.stateTime) * mag * .2);
        if (this.stateTime > 2) {
          this.state = (this.state == 'fireUp') ? 'down' : 'up';
        }
      }
    }
    super.update(scene, ms);
  }

  onCollisionEnter(other, coll) {
    if (other.label == 'player') {
      // why are you getting in melee range with the boss?
      other.inflict(2);
    }
  }

  onDamage() {
    assets.sounds.bossHit.play();
  }

  onDeath() {
    assets.sounds.bossDead.play();
    this.stateTime = 0;
    this.blinkTimer = 10000;
    GameManager.addScore(10000);
    GameManager.enemyDown();
    for (const other of GameManager.scene.objects) {
      if (other.label == 'enemy' && !other.dead) {
        other.selfDestruct();
      }
    }
  }
}


