import Point from '../Point';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';

const TAU = Math.PI * 2;

const prefab = {
  label: 'enemy',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'idle',
  hitbox: [-1.5, -1, 1.5, 1, 0x7],
  layer: 1,
  animations: {
    idle: [
      [assets.images.wasp, 0, 0, 96, 64, [1.5, 1]],
    ],
  },
};

const tempVector = new Point(0, 0);
const delta = new Point(0, 0);

export default class Wasp extends CharacterCore {
  constructor(origin) {
    super(prefab, origin);
    this.state = 'enter';
    this.stateTime = Math.random() * 2;
    this.direction = Math.random() > .5;
  }

  start() {
    this.health = 2;
    this.maxHealth = 2;
    this.speed = (5.0 + GameManager.difficulty) / 600;
    this.velocity = new Point(-1, 0);
    this.pivotX = 0;
    this.pivotY = 0;
    this.theta = null;
    this.shotCooldown = 1500 + (this._origin[0] - 15) * 100;
  }

  update(scene, ms) {
    this.stateTime += ms * .01;
    let mag = this.speed * ms;
    if (this.dead) {
      if (!this.didDrop) {
        this.didDrop = true;
        scene.add(new assets.prefabs.energy(this.origin, GameManager.hero));
      }
      this.move((Math.random() - .5) * mag, mag * 2);
      super.update(scene, ms);
      return;
    }
    if (this._origin[1] > 10) {
      delta[1] = -1;
    } else if (this._origin[1] < -10) {
      delta[1] = +1;
    } else {
      delta[1] = 0;
    }
    if (this.state == 'enter') {
      delta[0] = -.3;
      if (delta[1] == 0) {
        delta[1] = this._origin[1] * .01;
      }
    } else if (this._origin[0] > 14) {
      delta[0] = -1;
    } else if (this._origin[0] < 0) {
      delta[0] = +1;
    } else {
      delta[0] = 0;
    }
    if (this.state == 'enter') {
      if (this._origin[0] < 12 && this._origin[1] > -10 && this._origin[1] < 10) {
        this.state = 'circle';
        this.stateTime = Math.random() * 2;
      }
    } else if (this.state == 'circle') {
      if (delta[0] || delta[1]) {
        this.state = 'center';
        this.lingerTime = Math.random() * 250;
      } else {
        let cTheta;
        if (this.theta == null) {
          let targetX, targetY;
          do {
            targetX = (this._origin[0] > 5) ? Math.random() * 4 + 1 : Math.random() * 4 + 5;
            if (this.spawnedByBoss) {
              targetY = (this.direction ? -3 : 3) * (Math.random() + 1);
            } else {
              targetY = Math.sign(this._origin[1]) * Math.random() * -3;
            }
          } while (Math.abs(targetX - this._origin[0]) + Math.abs(targetY - this._origin[1]) < 4);
          const radius = Math.random() * 6 - 3;
          const cx = (targetX + this._origin[0]) * .5;
          const cy = (targetY + this._origin[1]) * .5;
          tempVector.setXY(
            targetX - this._origin[0],
            this._origin[1] - targetY,
          );
          tempVector.normalize();
          this.pivotX = cx + radius * tempVector[1];
          this.pivotY = cy + radius * tempVector[0];
          cTheta = (Math.atan2(this._origin[1] - this.pivotY, this._origin[0] - this.pivotX) + TAU) % TAU;
          if (this.direction) {
            this.theta = (cTheta + (Math.random() * .4 + .4) * Math.PI) % TAU;
          } else {
            this.theta = (cTheta - (Math.random() * .4 + .4) * Math.PI + TAU) % TAU;
          }
        } else {
          cTheta = (Math.atan2(this._origin[1] - this.pivotY, this._origin[0] - this.pivotX) + TAU) % TAU;
        }
        if (this.direction) {
          delta[0] = -Math.sin(cTheta);
          delta[1] = Math.cos(cTheta);
          if (cTheta > this.theta) this.theta = null;
        } else {
          delta[0] = Math.sin(cTheta);
          delta[1] = -Math.cos(cTheta);
          if (cTheta < this.theta) this.theta = null;
        }
      }
    } else if (this.state == 'center') {
      this.lingerTime -= ms;
      if (this.lingerTime <= 0) {
        this.state = 'circle';
        this.theta = null;
      }
      delta[0] = Math.sign(5 - this._origin[0]);
      delta[1] = -Math.sign(this._origin[1]);
    }
    for (const other of scene.objects) {
      if (other == this || other.label != 'enemy') continue;
      tempVector.set(this._origin);
      tempVector.subtract(other._origin);
      if (tempVector.magnitudeSquared < 4) {
        tempVector.normalize();
        delta.add(tempVector);
      }
    }
    delta.clampMagnitude(1);
    delta.subtract(this.velocity);
    delta.clampMagnitude(ms * .007);
    this.velocity[0] += delta[0];
    this.velocity[1] += delta[1];
    this.move(this.velocity[0] * mag, this.velocity[1] * mag);

    if (this.state != 'enter') {
      if (this.shotCooldown < 0) {
        this.shotCooldown = 1500;
        scene.add(new assets.prefabs.bullet([this._origin[0] - .4, this._origin[1]], -.02, 0, 'stinger'));
      } else {
        this.shotCooldown -= ms * (this.velocity[0] * .5 + 1);
      }
    }

    super.update(scene, ms);
  }

  onCollisionEnter(other, coll) {
    if (other.label == 'player') {
      other.inflict(1);
    }
  }

  onDeath() {
    if (!this.selfDestructed) {
      assets.sounds.mosquitoHit.play();
      GameManager.addScore(50);
    }
    this.stateTime = 0;
    if (!this.spawnedByBoss) {
      GameManager.enemyDown();
    }
  }
}
