import Point from '../Point';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';

const prefab = {
  label: 'enemy',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'idle',
  hitbox: [-7, -5, 9, 2, 0x7],
  layer: 1,
  animations: {
    idle: [
      [assets.images.waspBoss, 0, 0, 640, 428, [10, 428/64]],
    ],
  },
};

const tempVector = new Point(0, 0);

export default class WaspBoss extends CharacterCore {
  constructor(origin) {
    super(prefab, origin);
    this.state = 'enter';
    this.stateTime = Math.random() * 2;
    this.speed = 3.0 / 500;
    this.fireRate = 1000;
    this.maxHealth = 25;
  }

  start() {
    this.health = 25;
    this.speed = (5.0 + GameManager.difficulty) / 700;
    this.spawnCooldown = 0;
    this.fireCooldown = this.fireRate / 2;
    this.nextTop = true;
  }

  update(scene, ms) {
    if (this.state == 'merge') return; // handled by monstrosity
    this.stateTime += ms * .01;
    let mag = this.speed * ms;
    if (this.dead) {
      if (!this.didDrop) {
        this.didDrop = true;
        for (let i = 0; i < GameManager.hero.maxEnergy; i++) {
          tempVector[0] = this.lastAabb[0] + this.lastAabb.width * Math.random();
          tempVector[1] = this.lastAabb[1] + this.lastAabb.height * Math.random();
          scene.add(new assets.prefabs.energy(tempVector, GameManager.hero));
        }
      }
      this.stateTime += ms * .01;
      const swarmX = Math.sin(this.stateTime * 3) * mag - Math.cos(this.stateTime * .7) * mag * .5;
      this.move(swarmX * this.stateTime, mag);
      super.update(scene, ms);
      return;
    }
    let dy = Math.sin(this.stateTime * .7) * mag;
    if (this._origin[1] < -7) {
      dy = .2 * mag;
    } else if (this._origin[1] > 7) {
      dy = -.2 * mag;
    }
    let dx = 0;
    if (this.state == 'enter') {
      this.move(-mag * .8, dy);
      if (this._origin[0] < 10.5 && this._origin[1] > -5 && this._origin[1] < 5) {
        this.state = 'main';
        this.stateTime = Math.random() * 2;
      }
    } else {
      if (this._origin[0] > 13) {
        dx = -mag;
      } else if (this._origin[0] > 7) {
        dx -= .2 * mag;
      } else if (this._origin[0] < -12) {
        dx += .3 * mag
      }
      this.move(dx, dy);

      this.spawnCooldown -= ms;
      if (this.spawnCooldown < 0) {
        this.spawnCooldown = this.fireRate;
        const angle = (Math.random() + .5) * Math.PI;
        tempVector[0] = this._origin[0] + 2;
        tempVector[1] = this._origin[1] + this.nextTop ? -2 : 2;
        const wasp = new assets.prefabs.wasp(tempVector);
        wasp.spawnedByBoss = this;
        wasp.state = 'circle';
        wasp.direction = this.nextTop;
        scene.add(wasp);
        this.nextTop = !this.nextTop;
      }
      this.fireCooldown -= ms;
      if (this.fireCooldown < 0) {
        this.fireCooldown = this.fireRate;
        tempVector[0] = this._origin[0] + this.hitbox[0];
        tempVector[1] = this._origin[1] + this.hitbox[1] + (.25 + Math.random() * .5) * this.hitbox.height;
        scene.add(new assets.prefabs.bullet(tempVector, -.04, 0, 'stinger'));
      }
    }
    super.update(scene, ms);
  }

  onCollisionEnter(other, coll) {
    if (other.label == 'player') {
      // why are you getting in melee range with the boss?
      other.inflict(2);
    }
  }

  onDamage() {
    assets.sounds.bossHit.play();
  }

  onDeath() {
    assets.sounds.bossDead.play();
    this.stateTime = 0;
    GameManager.addScore(1000);
    GameManager.enemyDown();
    for (const other of GameManager.scene.objects) {
      if (other instanceof assets.prefabs.wasp) {
        other.selfDestruct();
      }
    }
  }
}

