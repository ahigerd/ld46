import Point from '../Point';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';

const prefab = {
  label: 'enemy',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'idle',
  hitbox: [-.5, -.5, 1, .5, 0x7],
  layer: 1,
  animations: {
    idle: [
      [assets.images.mosquito, 0, 0, 51, 32, [.5, .5]],
    ],
  },
};

const tempVector = new Point(0, 0);

export default class Mosquito extends CharacterCore {
  constructor(origin) {
    super(prefab, origin);
    this.state = 'enter';
    this.stateTime = Math.random() * 2;
    this.speed = 3.0 / 500;
    this.retreatDistance = 10;
    this.swarmTransition = 0.0001;
  }

  start() {
    this.health = 1;
    this.maxHealth = 1;
    this.speed = (5.0 + GameManager.difficulty) / 700;
    this.swarmTime = 25 - GameManager.difficulty * 5;
    if (this.swarmTime < 5) this.swarmTime = 5;
  }

  update(scene, ms) {
    this.stateTime += ms * .01;
    let mag = this.speed * ms;
    let swarmX = Math.sin(this.stateTime * 3) * mag - Math.cos(this.stateTime * .7) * mag * .5;
    if (this.dead) {
      if (!this.didDrop) {
        this.didDrop = true;
        scene.add(new assets.prefabs.energy(this.origin, GameManager.hero));
      }
      this.stateTime += ms * .01;
      this.move(swarmX * this.stateTime, mag * 2);
      super.update(scene, ms);
      return;
    }
    if (this._origin[0] > 13) {
      swarmX = -mag;
    } else if (this._origin[0] > 8) {
      swarmX -= .2 * mag;
    } else if (this._origin[0] < -12) {
      swarmX += .3 * mag
    }
    let swarmY = Math.sin(this.stateTime * 2.01) * mag * .25 + Math.sin(this.stateTime * .7) * mag;
    if (this._origin[1] < -5) {
      swarmY = .2 * mag;
    } else if (this._origin[1] > 5) {
      swarmY = -.2 * mag;
    }
    if (this.state == 'enter') {
      this.move(swarmX - mag * .5, swarmY);
      if (this._origin[0] < 10.5 && this._origin[1] > -5 && this._origin[1] < 5) {
        this.state = 'swarm';
        this.stateTime = Math.random() * 2;
      }
    } else if (this.state == 'swarm') {
      this.move(swarmX, swarmY);
      if (this.stateTime > this.swarmTime && Math.random() < (this.stateTime - this.swarmTime) * this.swarmTransition) {
        this.state = 'dive';
        this.stateTime = 0;
      }
    } else if (this.state == 'dive' || this.state == 'hover') {
      tempVector.set(GameManager.butterfly.origin);
      tempVector.subtract(this._origin);
      tempVector.normalize();
      if (this.state == 'hover') {
        swarmX *= .25;
        swarmY *= .25;
        if (this.stateTime > 2) {
          this.state = 'retreat';
          this.stateTime = 0;
          this.retreatDistance = 5 * Math.random() * 5;
        }
      }
      this.move(tempVector[0] * mag + swarmX, tempVector[1] * mag + swarmY * .5);
    } else if (this.state == 'retreat') {
      this.move(swarmX + 0.5 * mag, swarmY);
      if (this.origin.distanceTo(GameManager.butterfly.origin) > this.retreatDistance) {
        this.state = 'swarm';
        this.stateTime = Math.random() * 2;
      }
    }
    for (const other of scene.objects) {
      if (other == this || other.label != 'enemy') continue;
      tempVector.set(this._origin);
      tempVector.subtract(other._origin);
      if (tempVector.magnitudeSquared < 4) {
        tempVector.normalize();
        this.move(tempVector[0] * mag, tempVector[1] * mag);
      }
    }
    super.update(scene, ms);
  }

  onCollisionEnter(other, coll) {
    if (other.label == 'player') {
      other.inflict(1);
      if (this.state != 'hover') {
        this.state = 'hover';
        this.stateTime = 0;
      }
    }
  }

  onDeath() {
    if (!this.selfDestructed) {
      assets.sounds.mosquitoHit.play();
      GameManager.addScore(10);
    }
    this.stateTime = 0;
    if (!this.spawnedByBoss) {
      GameManager.enemyDown();
    }
  }
}
