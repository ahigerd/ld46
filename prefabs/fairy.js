import { Input } from '../Engine';
import Point from '../Point';
import Sprite from '../Sprite';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';
import CONFIG from '../config';

const ACCEL = 0.01;
const DECEL = 0.02;

export function sortObjectsByDepth(lhs, rhs) {
  if (lhs.label == 'bullet' && rhs.label != 'bullet') return 1;
  if (lhs.label != 'bullet' && rhs.label == 'bullet') return -1;
  if (lhs == GameManager.hero) return 1;
  if (rhs == GameManager.hero) return -1;
  return (rhs.plane - lhs.plane) || (lhs._origin[1] - rhs._origin[1]);
}

function accel(d, plus, minus, ms) {
  const c = (plus || 0) - (minus || 0);
  if (Math.abs(c - d) < .01) {
    return c;
  } else if (c == 0) {
    return d - d * ms * DECEL;
  } else {
    return d + (c - d) * ms * ACCEL;
  }
}

class Wings extends Sprite {
  constructor(fairy) {
    super({
      label: 'player',
      animateHitboxes: false,
      defaultIsAnimating: true,
      defaultAnimationName: 'idle',
      hitbox: [0, 0, 0, 0, 0],
      layer: 1,
      animations: {
        idle: [
          [assets.images.wings1, 0, 0, 45, 64, [1.9, 1.8]],
          [assets.images.wings2, 0, 0, 45, 64, [1.9, 1.8]],
          125,
        ],
        move: [
          [assets.images.wings1, 0, 0, 45, 64, [1.9, 1.8]],
          [assets.images.wings2, 0, 0, 45, 64, [1.9, 1.8]],
          75,
        ],
        dead: [
          [assets.images.wings1, 0, 0, 45, 64, [1.9, 1.8]],
        ],
      },
    }, fairy.origin)
    this.fairy = fairy;
  }

  update(scene, ms) {
    this.origin.set(this.fairy.origin);
    if (this.currentAnimationName != this.fairy.currentAnimationName) {
      this.setAnimation(this.fairy.currentAnimationName, true);
    }
  }
}

export default class Fairy extends CharacterCore {
  constructor(origin) {
    super({
      label: 'player',
      animateHitboxes: false,
      defaultIsAnimating: true,
      defaultAnimationName: 'idle',
      hitbox: [-.5, -1.5, .25, .7, 0x11],
      layer: 1,
      animations: {
        idle: [
          [assets.images.fairy1, 0, 0, 45, 96, [.6875, 1.5]],
        ],
        move: [
          [assets.images.fairy2, 0, 0, 45, 96, [.6875, 1.5]],
        ],
        dead: [
          [assets.images.fairy1, 0, 0, 45, 96, [.6875, 1.5]],
        ],
      },
    }, origin);
    this.speed = 5.0 / 500;
    this.velocity = new Point(0, 0);
    this.shotCooldown = 0;
    this.wings = new Wings(this);
    this.musicFadeTime = 0;
  }

  start() {
    this.maxHealth = CONFIG.easyMode ? 10 : 5;
    this.health = .1;
    this.energy = 0;
    this.maxEnergy = CONFIG.easyMode ? 5 : 20;
    this.bombs = CONFIG.easyMode ? 3 : 1;
    this.maxBombs = 4;
    this.bombCooldown = 0;
    this.energyFlash = 0;
    this.scene.add(this.wings);
  }

  update(scene, ms) {
    if (this.dead) {
      if (this.blinkTimer <= 0) {
        this.GameManager.gameOver();
      }
      this.blinkTimer -= ms * .5;
      assets.sounds.bgmLoop.volume = this.blinkTimer > 0 ? (this.blinkTimer / 2500) : 0;
      this.move((Math.random() - .5) * ms * .01, ms * .01);
      super.update(scene, ms);
      return;
    }

    if (this.regen) {
      let damaged = 2;

      this.health += ms * .0004 * this.maxHealth;
      if (this.health > this.maxHealth) {
        this.health = this.maxHealth;
        --damaged;
      }

      const { butterfly } = GameManager;
      butterfly.health += ms * .0004 * butterfly.maxHealth;
      if (butterfly.health > butterfly.maxHealth) {
        butterfly.health = butterfly.maxHealth;
        --damaged;
      }

      if (damaged < 1) this.regen = false;
    }

    this.velocity[0] = accel(this.velocity[0], Input.keys.ArrowRight, Input.keys.ArrowLeft, ms);
    this.velocity[1] = accel(this.velocity[1], Input.keys.ArrowDown, Input.keys.ArrowUp, ms);
    const mag2 = this.velocity.magnitudeSquared;
    if (mag2 > 1) {
      this.velocity.normalize();
    }
    this.setAnimation(mag2 > .01 ? 'move' : 'idle');
    const mag = this.speed * ms;
    this.move(this.velocity[0] * mag, this.velocity[1] * mag);

    if (this.musicFadeTime) {
      if (this.musicFadeTime < 0) this.musicFadeTime = 0;
      assets.sounds.bgmLoop.volume = this.musicFadeTime / 3000;
      this.musicFadeTime -= ms;
    } else if (GameManager.enemiesRemaining > 0) {
      this._doBombs(scene, ms);
      this._doShoot(scene, ms);
    }

    const screen = GameManager.camera.aabb;
    let { height, width } = this.hitbox;
    width *= .5;
    if (this._origin[0] < screen[0] + width) {
      this._origin[0] = screen[0] + width;
    } else if (this._origin[0] > screen[2] - width) {
      this._origin[0] = screen[2] - width;
    }
    if (this._origin[1] < screen[1] + height + screen.height * .05) {
      this._origin[1] = screen[1] + height + screen.height * .05;
    } else if (this._origin[1] > screen[3]) {
      this._origin[1] = screen[3];
    }

    super.update(scene, ms);
  }

  _calcInertia(plus, minus, last) {
    const next = (((plus || 0) - (minus || 0)) + last + last) / 3;
    if (Math.abs(next - last) > .5) {
      return Math.sign(next) * .5;
    }
    return next;
  }

  onCollisionEnter(other, coll) {
    this.onCollisionStay(other, coll);
  }

  onCollisionStay(other) {
  }

  onDamage() {
    assets.sounds.fairyHit.play();
  }

  onDeath() {
    assets.sounds.gameOver.play();
  }

  lateUpdate(scene) {
    scene.objects.sort(sortObjectsByDepth);
  }

  render(camera) {
    super.render(camera);
    const layer = camera.layers[this.layer];
    const fraction = this.energy < 0 ? 0 : this.energy / this.maxEnergy;
    const pixelRect = this.pixelRect;
    const width = pixelRect[2] - pixelRect[0] - .5;
    if (this.bombs >= this.maxBombs && this.energy >= this.maxEnergy && this.energyFlash > 120) {
      layer.fillStyle = '#FFFF00';
    } else {
      layer.fillStyle = '#00FFFF';
    }
    layer.fillRect(pixelRect[0], pixelRect[1] - 10, width * fraction, 3);
    for (let i = 1; i <= this.bombs; i++) {
      if ((this.energyFlash + i * 33) % 150 > 120) {
        layer.fillStyle = '#FFFF00';
      } else {
        layer.fillStyle = '#00FFFF';
      }
      layer.fillRect(pixelRect[0], pixelRect[1] - 8 - i - i, width, 1);
    }
  }

  zapTouching(scene, damage = 1) {
    for (const other of scene.objects) {
      if (other.label == 'enemy' && this.lastAabb.intersects(other.lastAabb)) {
        // You deal damage to anything touching you when you fire
        other.inflict(damage);
      }
    }
  }

  win() {
    this.musicFadeTime = 2000;
  }

  _doBombs(scene, ms) {
    while (this.energy >= this.maxEnergy) {
      if (this.bombs >= this.maxBombs) {
        this.energy = this.maxEnergy;
        break;
      }
      this.energy -= this.maxEnergy;
      this.bombs++;
    }
    if (this.bombCooldown > 0) this.bombCooldown -= ms;
    if (this.bombs > 0) {
      this.energyFlash = (this.energyFlash + ms) % 150;
      if (Input.keys.Shift && this.bombCooldown <= 0) {
        this.bombCooldown = 2000;
        --this.bombs;
        assets.sounds.bomb.play();
        if (this.health < this.maxHealth) {
          this.health++;
          setTimeout(() => assets.sounds.heal.play(), 100);
        } else if (GameManager.butterfly.health < GameManager.butterfly.maxHealth) {
          scene.add(new assets.prefabs.energy([this._origin[0] + .5, this._origin[1] - .875], GameManager.butterfly, 'big'));
        }
        const angle = Math.PI / 15 * 8;
        for (let i = 0; i < 15; i++) {
          scene.add(new assets.prefabs.bullet(this._origin, angle * i, -.5 + Math.floor(i / 3) * .5, 'flower' + (i % 3 + 1)));
        }
        this.zapTouching(scene, 2);
      }
    }
  }

  _doShoot(scene, ms) {
    if (Input.keys[' '] && this.shotCooldown <= 0) {
      this.zapTouching(scene);
      assets.sounds.shoot.play();
      scene.add(new assets.prefabs.bullet([this._origin[0] + .5, this._origin[1] - .875], .025, 0));
      if (this.velocity[0] < -.1) {
        this.shotCooldown = 100;
      } else if (this.velocity[0] > .1) {
        this.shotCooldown = 200;
      } else {
        this.shotCooldown = 140;
      }
    } else if (this.shotCooldown > 0) {
      this.shotCooldown -= ms;
    }
  }
}
