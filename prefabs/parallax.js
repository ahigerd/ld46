import assets from '../scripts/assets';
import Sprite from '../Sprite';

const PLANE_SPEED = .1;
const BOSS_MODIFIER = -.07;
const TRANSITION_SPEED = 1/3000;
// To avoid recomputation, the first parallax object to be constructed tracks transition values
let speed = -PLANE_SPEED;
let transition = 0;

const prefab = {
  animateHitboxes: true,
  defaultIsAnimating: false,
  defaultAnimationName: 'layer1',
  hitbox: [-1, -1, 1, 1, 0],
  hasCollision: false,
  layer: 0,
  animations: {
    layer01: [
      [assets.images.grass3, 0, 0, 235, 96, [0, 3], [0, 0, 235/32, 3, 0]],
    ],
    layer02: [
      [assets.images.grass3, 0, 0, 235, 96, [0, 3], [0, 0, 235/32, 3, 0]],
    ],
    layer03: [
      [assets.images.grass3, 0, 0, -235, 96, [0, 3], [0, 0, 235/32, 3, 0]],
    ],
    layer11: [
      [assets.images.mushroom1b, 0, 0, 320, 320, [0, 10], [0, 0, 10, 10, 0]],
    ],
    layer12: [
      [assets.images.mushroom2b, 0, 0, 200, 160, [0, 5], [0, 0, 6.25, 5, 0]],
    ],
    layer13: [
      [assets.images.mushroom2b, 0, 0, -200, 160, [0, 4.8], [0, 0, 6.25, 5, 0]],
    ],
    layer21: [
      [assets.images.grass1a, 0, 0, 800, 365, [0, 365/32], [0, 0, 25, 365/32, 0]],
    ],
    layer22: [
      [assets.images.grass1a, 0, 0, 800, 365, [0, 365/32], [0, 0, 25, 365/32, 0]],
    ],
    layer23: [
      [assets.images.grass1a, 0, 0, -800, 365, [0, 365/32], [0, 0, 25, 365/32, 0]],
    ],
    layer31: [
      [assets.images.grass2a, 0, 0, 1280, 584, [0, 584/32], [0, 0, 40, 584/32, 0]],
    ],
    layer32: [
      [assets.images.grass2a, 0, 0, -1280, 584, [0, 584/32], [0, 0, 40, 584/32, 0]],
    ],
    layer33: [
      [assets.images.grass2a, 0, 0, -1280, 584, [0, 584/32], [0, 0, 40, 584/32, 0]],
    ],
  },
};

export default class ParallaxItem extends Sprite {
  constructor(plane, origin) {
    super(prefab, origin);
    this.plane = plane;
    this.transition = 0;
    if (GameManager.parallax.length == 1 && GameManager.parallax[0].length == 0) {
      this.first = true;
    }
  }

  update(scene, ms) {
    if (this.first) {
      if (GameManager.bossWave) {
        transition = (transition < 1) ? transition + ms * TRANSITION_SPEED : 1;
      } else {
        transition = (transition > 0) ? transition - ms * TRANSITION_SPEED : 0;
      }
      speed = -(PLANE_SPEED + BOSS_MODIFIER * transition);
      if (GameManager.finalWave) {
        GameManager.camera.canvases[1].style.backgroundColor = 'rgba(64, 0, 0, ' + (transition * .7) + ')';
      } else {
        GameManager.camera.canvases[1].style.backgroundColor = 'rgba(0, 0, 0, ' + (transition * .4) + ')';
      }
    }
    this.move(speed / (this.plane || .9), 0);
  }

  lateUpdate(scene, force) {
    const screen = GameManager.camera.aabb;
    if (this._origin[0] <= screen[0] - this.hitbox[2]) {
      this.setAnimation('layer' + this.plane + '' + Math.floor(Math.random() * 3 + 1), true);
      while (this.previousElement._origin[0] + this.previousElement.hitbox[2] < screen[2]) {
        // The right edge of the previous one is still on-screen, which means there aren't
        // enough elements to fill the scrolling area
        const newElement = new ParallaxItem(this.plane, this._origin);
        newElement.setAnimation('layer' + this.plane + '' + Math.floor(Math.random() * 3 + 1));
        newElement._origin[0] = this.previousElement._origin[0] + this.previousElement.hitbox[2] + (this.plane == 1 ? Math.random() * 10 : 0);
        newElement.previousElement = this.previousElement;
        GameManager.parallax[this.plane].push(newElement);
        scene.add(newElement);
        this.previousElement = newElement;
      }
      this._origin[0] = this.previousElement._origin[0] + this.previousElement.hitbox[2] + (this.plane == 1 ? Math.random() * 10 : 0);
    }
  }
}
