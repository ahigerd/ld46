import Point from '../Point';
import CharacterCore from '../scripts/CharacterCore';
import assets from '../scripts/assets';
import GameManager from '../scripts/GameManager';

const prefab = {
  label: 'enemy',
  animateHitboxes: false,
  defaultIsAnimating: true,
  defaultAnimationName: 'idle',
  hitbox: [-12, -3.5, 9, 3, 0x7],
  layer: 1,
  animations: {
    idle: [
      [assets.images.dragonflyBoss, 0, 0, 800, 549, [12.5, 549/64]],
    ],
  },
};

const tempVector = new Point(0, 0);

export default class DragonflyBoss extends CharacterCore {
  constructor(origin) {
    super(prefab, origin);
    this.state = 'enter';
    this.maxHealth = 25;
  }

  start() {
    this.health = 25;
    this.speed = (5.0 + GameManager.difficulty) / 700;
    this.stateTime = 0;
  }

  update(scene, ms) {
    if (this.state == 'merge') return; // handled by monstrosity
    this.stateTime += ms * .01;
    let mag = this.speed * ms;
    if (this.dead) {
      if (!this.didDrop) {
        this.didDrop = true;
        for (let i = 0; i < GameManager.hero.maxEnergy; i++) {
          tempVector[0] = this.lastAabb[0] + this.lastAabb.width * Math.random();
          tempVector[1] = this.lastAabb[1] + this.lastAabb.height * Math.random();
          scene.add(new assets.prefabs.energy(tempVector, GameManager.hero));
        }
      }
      this.stateTime += ms * .01;
      const swarmX = Math.sin(this.stateTime * 3) * mag - Math.cos(this.stateTime * .7) * mag * .5;
      this.move(swarmX * this.stateTime, mag);
      super.update(scene, ms);
      return;
    }
    let dx = 0, dy = 0;
    if (this.state == 'enter') {
      this.move(-mag * .8, Math.sin(this.stateTime) * mag * .2);
      if (this._origin[0] < 12) {
        this.state = Math.random() > .5 ? 'up' : 'down';
      }
    } else {
      if (this.state == 'up') {
        this.move(0, -mag);
        if (this._origin[1] < -5) {
          this.state = 'fireUp';
          this.stateTime = 0;
          const dragonfly = new assets.prefabs.dragonfly([20, Math.random() * -10]);
          dragonfly.spawnedByBoss = true;
          scene.add(dragonfly);
        }
      } else if (this.state == 'down') {
        this.move(0, mag);
        if (this._origin[1] > 5) {
          this.state = 'fireDown';
          this.stateTime = 0;
          const dragonfly = new assets.prefabs.dragonfly([20, Math.random() * 10]);
          dragonfly.spawnedByBoss = true;
          scene.add(dragonfly);
        }
      } else {
        this.move(0, Math.sin(this.stateTime) * mag * .2);
        if (this.stateTime > 10) {
          this.state = (this.state == 'fireUp') ? 'down' : 'up';
        }
      }
    }
    super.update(scene, ms);
  }

  onCollisionEnter(other, coll) {
    if (other.label == 'player') {
      // why are you getting in melee range with the boss?
      other.inflict(2);
    }
  }

  onDamage() {
    assets.sounds.bossHit.play();
  }

  onDeath() {
    assets.sounds.bossDead.play();
    this.stateTime = 0;
    GameManager.addScore(1000);
    GameManager.enemyDown();
    for (const other of GameManager.scene.objects) {
      if (other instanceof assets.prefabs.dragonfly) {
        other.selfDestruct();
      }
    }
  }
}

