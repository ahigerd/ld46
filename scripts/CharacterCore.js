import Sprite from '../Sprite';
import { PIXELS_PER_UNIT } from '../Point';
import GameManager from '../scripts/GameManager';

export default class CharacterCore extends Sprite {
  constructor(config, origin) {
    super(config, origin);
    this.GameManager = GameManager;
  }

  shouldCollide(other) {
    // TODO: profile `return other instanceof CharacterCore;`
    return false;
  }

  selfDestruct() {
    this.selfDestructed = true;
    this.blinkTimer = 0;
    this.inflict(this.health);
  }

  inflict(damage) {
    if (this.immune || this.blinkTimer > 0) return;
    this.health -= damage;
    if (this.health <= 0) {
      this.health = 0;
      this.dead = true;
      this.hasCollision = false;
      if (this.animations.dead)
        this.setAnimation('dead');
      this.blinkTimer = this.maxHealth > 10 || this.label != 'enemy' ? 2500 : 500;
      this.onDeath();
    } else {
      this.blinkTimer = this.maxHealth > 10 ? 500 : (this.label == 'enemy' ? 300 : 1000);
      this.onDamage();
    }
  }

  render(camera) {
    if (this.forceShowHealth && this.hidden) this.renderHealth(camera);
    if (this.hidden) return;
    if (this.maxHealth > 1) this.renderHealth(camera);
    super.render(camera);
  }

  renderHealth(camera) {
    const layer = camera.layers[this.layer];
    const fraction = this.health < 0 ? 0 : this.health / this.maxHealth;
    const pixelRect = this.pixelRect;
    layer.fillStyle = `hsl(${fraction * 120},100%,50%)`;
    layer.fillRect(pixelRect[0], pixelRect[1] - 5, (pixelRect[2] - pixelRect[0] - .5) * fraction, 3);
  }

  start() {
    this.lastDir = 'down';
    this.health = 100;
    this.maxHealth = 100;
    this.hidden = false;
    this.dead = false;
    this.blinkTimer = 0;
  }

  moveColliding(ms, dx, dy) {
    const tilemap = this.GameManager.tilemap;
    const moving = dx || dy;
    if (dy < 0) {
      this.lastDir = 'up';
    } else if (dy > 0) {
      this.lastDir = 'down';
    } else if (dx < 0) {
      this.lastDir = 'left';
    } else if (dx > 0) {
      this.lastDir = 'right';
    }
    if (dx || dy) {
      const bits = this.hitbox.bits;
      ms /= 500;
      dx *= ms;
      dy *= ms;
      if (dx && dy) {
        dx *= .71;
        dy *= .71;
      }
      const rx = this._origin[0] + this.hitbox[dx < 0 ? 0 : 2] + dx;
      const ry = this._origin[1] + this.hitbox[dy < 0 ? 1 : 3] + dy;
      if (dx) {
        if (tilemap.bitsAt(rx, this._origin[1] + this.hitbox[1]) & bits || tilemap.bitsAt(rx, this._origin[1] + this.hitbox[3]) & bits) {
          dx = 0;
        }
      }
      if (dy) {
        if (tilemap.bitsAt(this._origin[0] + this.hitbox[0], ry) & bits || tilemap.bitsAt(this._origin[0] + this.hitbox[2], ry) & bits) {
          dy = 0;
        }
      }
      const tileBits = tilemap.bitsAt(rx, ry);
      if (tileBits & 2) dx *= .5;
      if (tileBits & 4) dy *= .5;
      this.move(dx, dy);
      if (dy < 0 && (dy < -Math.abs(dx))) {
        this.lastDir = 'up';
      } else if (dy > 0 && (dy > Math.abs(dx))) {
        this.lastDir = 'down';
      } else if (dx < 0 && (dx < -Math.abs(dy))) {
        this.lastDir = 'left';
      } else if (dx > 0 && (dx > Math.abs(dy))) {
        this.lastDir = 'right';
      }
    }
    if (moving) {
      this.setAnimation(this.lastDir);
      return dx || dy;
    } else {
      this.setAnimation('stand_' + this.lastDir);
      return false;
    }
  }

  centerCameraOn(sprite) {
    const camera = this.GameManager.camera;
    const tilemap = this.GameManager.tilemap;
    const halfWidth = camera.width * .5;
    const halfHeight = camera.height * .5;
    let x = ((sprite._origin[0] * PIXELS_PER_UNIT) | 0) / PIXELS_PER_UNIT;
    let y = ((sprite._origin[1] * PIXELS_PER_UNIT) | 0) / PIXELS_PER_UNIT;
    if (x - halfWidth < tilemap.lastAabb[0]) {
      x = tilemap.lastAabb[0] + halfWidth;
    } else if (x + halfWidth > tilemap.lastAabb[2]) {
      x = tilemap.lastAabb[2] - halfWidth;
    }
    if (y - halfHeight < tilemap.lastAabb[1]) {
      y = tilemap.lastAabb[1] + halfHeight;
    } else if (y + halfHeight > tilemap.lastAabb[3]) {
      y = tilemap.lastAabb[3] - halfHeight;
    }
    camera.setXY(x, y);
  }

  update(scene, ms) {
    if (this.blinkTimer > 0) {
      this.blinkTimer -= ms;
      this.hidden = this.blinkTimer <= 0 ? false : (this.blinkTimer % 100 < 35);
      if (this.dead && this.blinkTimer <= 0) {
        if (this.label !== 'hero') {
          scene.remove(this);
        }
      }
    }
  }

  onDamage() {}
  onDeath() {}
}
