import assets from './assets';
import Engine from '../Engine';
import Camera from '../Camera';
import { PIXELS_PER_UNIT } from '../Point';
import { sortObjectsByDepth } from '../prefabs/fairy';
import Scene from '../Scene';
import TouchControls from '../TouchControls';
import WaveDescription from './WaveDescription';
import CONFIG from '../config';

const INTRO_LENGTH_SEC = 149279 / 44100; // 149279 samples @ 44kHz
let audioLatency = 0, lastAudioTime = 0;

let state = 'title';
let score = 0;
let highScore;
try {
  highScore = localStorage.highScore | 0;
} catch (e) {
  highScore = 0;
}
const scoreSpan = document.getElementById('score');
const highScoreSpan = document.getElementById('highScore');
highScoreSpan.innerText = highScore;

function spawnTooClose(x, y, scene) {
  for (const obj of scene.nextObjects) {
    if (obj.isTileMap) continue;
    const dx2 = Math.sqrt(x/PIXELS_PER_UNIT - obj._origin[0]);
    const dy2 = Math.sqrt(y/PIXELS_PER_UNIT - obj._origin[1]);
    if (dx2 + dy2 < 1) return true;
  }
  for (const obj of scene.objects) {
    if (obj.isTileMap) continue;
    const dx2 = Math.sqrt(x/PIXELS_PER_UNIT - obj._origin[0]);
    const dy2 = Math.sqrt(y/PIXELS_PER_UNIT - obj._origin[1]);
    if (dx2 + dy2 < 1) return true;
  }
  return false;
}

const fullscreenEvents = ['fullscreenchange', 'fullscreenerror', 'msfullscreenchange', 'msfullscreenerror', 'webkitfullscreenchange', 'webkitfullscreenerror'];

const GameManager = {
  currentLevel: 0,
  currentWave: 0,
  difficulty: 0,
  bossWave: false,
  enemiesRemaining: 0,
  domLayers: {},
  engine: new Engine({ showFps: true }),
  camera: new Camera(document.getElementById('camera'), 960, 720, 2),
  disableFullscreen: false,

  init(assets) {
    this.assets = assets;
    assets.sounds.bgmLoop.loop = true;
    assets.sounds.bgmIntro.addEventListener('timeupdate', () => {
      const offset = assets.sounds.bgmIntro.currentTime - INTRO_LENGTH_SEC;
      if (offset > 0 && assets.sounds.bgmLoop.paused) {
        assets.sounds.bgmLoop.play();
        // XXX: background music needs work
        assets.sounds.bgmLoop.currentTime = offset + .13;
        assets.sounds.bgmIntro.pause();
      } else {
        audioLatency = (assets.sounds.bgmIntro.currentTime - lastAudioTime);
        lastAudioTime = assets.sounds.bgmIntro.currentTime;
      }
    });

    this.domLayers = {};
    for (const id of ['loading', 'splash', 'helppage', 'gameOver', 'gameOverWin', 'statusBar', 'pauseBox', 'gameOverStartButton', 'gameOverStartButton2', 'startButton', 'easyButton', 'helpButton', 'helpBackButton', 'resumeButton', 'fullscreen', 'butterflyAnimation', 'winScreen']) {
      this.domLayers[id] = document.getElementById(id);
    }

    this.bindEvent(this.domLayers.startButton, 'click', this.bindFullscreenThen(this.newGameNormal));
    this.bindEvent(this.domLayers.easyButton, 'click', this.bindFullscreenThen(this.newGameEasy));
    this.bindEvent(this.domLayers.helpButton, 'click', this.showHelp);
    this.bindEvent(this.domLayers.helpBackButton, 'click', this.showTitle);
    this.bindEvent(this.domLayers.gameOverStartButton, 'click', this.showTitle);
    this.bindEvent(this.domLayers.gameOverStartButton2, 'click', this.showTitle);
    this.bindEvent(this.domLayers.resumeButton, 'click', this.bindFullscreenThen(() => this.engine.start()));

    this.camera.setXY(0, 0);
    this.camera.setScale(1, 1);
    this.camera.layers[0].font = '16px sans-serif';
    this.camera.layers[0].textAlign = 'center';
    this.camera.layers[0].textBaseline = 'middle';
    this.engine.cameras.push(this.camera);

    this.engine.addEventListener('enginekeydown', e => e.detail.key === 'Escape' && this.engine.pause());
    this.engine.addEventListener('enginepause', this.onPause.bind(this));
    this.bindEvent(this.engine, 'enginestart', this.onStart.bind(this));

    this.touchControls = new TouchControls(
      document.getElementById('dpad'),
      document.getElementById('buttons'),
      document.getElementById('pauseContainer'),
      [
        { label: '<img src="assets/flower1a.png" />', key: 'Shift' },
        { label: '<img src="assets/fireball-24.png" />', key: ' ' },
      ],
    );
    this.touchControls.onPauseClicked = () => state === 'playing' && this.engine.pause();
    this.touchControls.hidden = true;

    this._spawnWave = this._spawnWave.bind(this);
    this.waitForClear = this.waitForClear.bind(this);

    GameManager.showTitle();
    // XXX: hackaroung because I can't figure out why the parallax doesn't work
    window.setTimeout(() => GameManager.showTitle(), 0);
  },

  bindEvent(target, eventName, callback, options = undefined) {
    target.addEventListener(eventName, (event) => {
      event.preventDefault();
      callback.call(this);
    }, options);
  },

  showTitle() {
    state = 'title';

    this.scene = this.engine.activeScene = new Scene();

    this.domLayers.loading.style.display = 'none';
    this.domLayers.splash.style.display = 'block';
    this.domLayers.helppage.style.display = 'none';
    this.domLayers.gameOver.style.display = 'none';
    this.domLayers.gameOverWin.style.display = 'none';
    this.domLayers.statusBar.className = 'onTitle';
    this.domLayers.easyButton.style.display = CONFIG.easyModeUnlocked ? '' : 'none';

    this.camera.setXY(0, 0);
    //  TODO: Not sure why the engine has to be ticked twice to make this work
    this._buildParallax();
    this.engine.step();
    this.scene.objects.sort(sortObjectsByDepth);
    this.camera.render(this.scene);
  },

  showHelp() {
    state = 'help';

    this.domLayers.splash.style.display = 'none';
    this.domLayers.helppage.style.display = 'block';
  },

  bindFullscreenThen(callback) {
    return () => {
      if (this.disableFullscreen) {
        callback.call(this);
        return;
      }
      this.fullscreenCallback = (event) => {
        if (event.type.toLowerCase().includes('error')) {
          this.disableFullscreen = true;
        }
        for (const eventType of fullscreenEvents) {
          fullscreen.removeEventListener(eventType, this.fullscreenCallback);
        }
        this.fullscreenCallback = null;
        callback.call(this);
      };

      const fullscreen = this.domLayers.fullscreen;
      for (const eventType of fullscreenEvents) {
        fullscreen.addEventListener(eventType, this.fullscreenCallback);
      }
      const requestFullscreen = fullscreen.requestFullscreen ||
        fullscreen.webkitRequestFullscreen ||
        fullscreen.msRequestFullscreen ||
        fullscreen.mozRequestFullScreen;
      if (!requestFullscreen) {
        this.disableFullscreen = true;
        callback.call(this);
        return;
      }
      requestFullscreen.call(fullscreen, { navigationUI: 'hide' });
    };
  },

  exitFullscreen() {
    if (!document.fullscreenElement && !document.fullscreen) {
      return false;
    }
    const exitFullscreen = document.exitFullscreen ||
      document.webkitExitFullscreen ||
      document.msExitFullscreen ||
      document.mozExitFullScreen;
    exitFullscreen.call(document);
  },

  newGameEasy() {
    CONFIG.easyMode = true;
    this.newGame();
  },

  newGameNormal() {
    CONFIG.easyMode = false;
    this.newGame();
  },

  newGame() {
    state = 'playing';
    this.domLayers.splash.style.display = 'none';
    this.domLayers.gameOver.style.display = 'none';
    this.domLayers.gameOverWin.style.display = 'none';
    this.domLayers.statusBar.className = 'inGame';
    this.scene = this.engine.activeScene = new Scene();

    this.hero = new assets.prefabs.fairy([-8, 0]);
    this.scene.add(this.hero);
    this.camera.setXY(0, 0);

    this.butterfly = new assets.prefabs.butterfly([-9.5, 0]);
    this.scene.add(this.butterfly);

    this.difficulty = 0;
    this.currentLevel = 0;
    this.newLevel();
    this._buildParallax();
    this.engine.start();
    assets.sounds.bgmIntro.currentTime = 0;
    assets.sounds.bgmLoop.currentTime = 0;
    assets.sounds.bgmIntro.play();
  },

  newLevel() {
    this.currentLevel++;
    this.currentWave = 0;
    this.enemiesRemaining = 0;
    this.bossWave = false;

    if (WaveDescription.length < this.currentLevel) {
      this.waitForClear();
      return;
    }

    this.newWave();
  },

  newWave() {
    if (this.bossWave) {
      // Just beat the boss wave? Next level!
      this.newLevel();
      return;
    }
    this.currentWave++;
    this.bossWave = (this.currentWave == WaveDescription[this.currentLevel - 1].length);
    this.finalWave = this.bossWave && this.currentLevel == WaveDescription.length;
    this.hero.regen = true;
    this.engine.enqueue(3000, this._spawnWave);
  },

  _spawnWave() {
    const before = this.scene.nextObjects.length;
    const wave = WaveDescription[this.currentLevel - 1][this.currentWave - 1];
    this.enemiesRemaining = 0;
    this.difficulty = wave(this.scene);
    this.enemiesRemaining += this.scene.nextObjects.length - before;
  },

  onPause() {
    if (!assets.sounds.bgmIntro.paused) {
      this.pausedMusic = assets.sounds.bgmIntro;
      assets.sounds.bgmIntro.pause();
    } else if (!assets.sounds.bgmLoop.paused) {
      this.pausedMusic = assets.sounds.bgmLoop;
      assets.sounds.bgmLoop.pause();
    }
    if (state == 'playing') {
      this.exitFullscreen();
      this.domLayers.pauseBox.style.display = 'block';
      this.touchControls.hidden = true;
    }
  },

  onStart() {
    if (state == 'playing') {
      this.domLayers.pauseBox.style.display = 'none';
      if (this.touchControls.touchEnabled) {
        this.touchControls.hidden = false;
      }
      if (this.pausedMusic) {
        this.pausedMusic.play();
      }
    } else {
      this.engine.pause(false);
    }
  },

  gameOver() {
    CONFIG.easyModeUnlocked = true;
    try {
      localStorage.easy = true;
    } catch (err) {
      // pass
    }
    assets.sounds.bgmIntro.pause();
    assets.sounds.bgmLoop.pause();
    this.exitFullscreen();
    state = 'gameover';
    if (WaveDescription.length < this.currentLevel) {
      this.domLayers.gameOverWin.style.display = 'block';
      this.butterflyFrame = -1;
      if (this.butterflyTimer) {
        window.clearInterval(this.butterflyTimer);
      }
      this.butterflyTimer = window.setInterval(() => {
        this.butterflyFrame = (this.butterflyFrame + 1) % 4;
        const size = this.domLayers.winScreen.offsetHeight / 600.0 * 128.0;
        console.log(size);
        this.domLayers.butterflyAnimation.style.height = size + "px";
        this.domLayers.butterflyAnimation.style.width = size + "px";
        this.domLayers.butterflyAnimation.style.backgroundImage = 'url("assets/butterfly-win' + (this.butterflyFrame + 1) + '.png")';
        this.domLayers.butterflyAnimation.style.top = (this.domLayers.winScreen.offsetTop + size / 7)  + "px";
        this.domLayers.butterflyAnimation.style.left = (this.domLayers.winScreen.offsetLeft + size)  + "px";
      }, 250);
    } else {
      this.domLayers.gameOver.style.display = 'block';
    }
  },

  addScore(points) {
    GameManager.setScore(score + points);
  },

  setScore(points) {
    score = points;
    scoreSpan.innerText = points;
    if (score > highScore) {
      highScore = score;
      highScoreSpan.innerText = highScore;
      try {
        localStorage.highScore = highScore;
      } catch (e) {
        // consume
      }
    }
  },

  _buildParallax() {
    if (this.parallax && this.parallax.length) {
      for (const plane of this.parallax) {
        for (const element of plane) {
          element.previousElement = null;
          this.scene.remove(element);
        }
      }
    }

    const [ left, , right, bottom ] = this.camera.aabb;
    this.parallax = [];
    for (let plane = 0; plane <= 3; plane++) {
      const planeObjects = [];
      this.parallax.push(planeObjects);
      const first = new assets.prefabs.parallax(plane, [left, bottom]);
      const choice = Math.floor(Math.random() * 3) + 1;
      first.isAsleep = false;
      first.setAnimation('layer' + plane + choice, true);
      first.previousElement = first;
      planeObjects.push(first);
      this.scene.add(first);
      first._origin[0] = left - first.hitbox.width;
      first.computeBoxes();
      first.lateUpdate(this.scene);
    }
  },

  enemyDown() {
    this.enemiesRemaining--;
    if (this.enemiesRemaining <= 0) {
      this.newWave();
    }
  },

  waitForClear() {
    for (const obj of this.scene.objects) {
      if (obj.label == 'enemy') {
        this.engine.enqueue(100, this.waitForClear);
        return;
      }
    }
    this.hero.win();
    this.engine.enqueue(2000, () => this.gameOver());
  },
};

window.GameManager = GameManager;

export default GameManager;
