import GameManager from './GameManager';
import assets from './assets';

function backMosquitos(scene) {
  for (let i = -5; i <= 5; i += 1) {
    scene.add(new assets.prefabs.mosquito([15 + 5 * Math.abs(i % 3), i]));
  }
}

function sideMosquitos(scene, pairs, addDeferred, advance = 0) {
  for (let i = 0; i < pairs; i += .5) {
    scene.add(new assets.prefabs.mosquito([14 - i - advance, 11 + i * .2]));
    scene.add(new assets.prefabs.mosquito([14 - i - advance, -11 - i * .2]));
    if (addDeferred) GameManager.enemiesRemaining += 2;
  }
}

function backWasps(scene, count, delay = 0, spread = 0) {
  for (let i = 0; i < count; i++) {
    spread++;
    if (i % 2) {
      scene.add(new assets.prefabs.wasp([15 + delay + i, spread * 2 + 1]));
    } else {
      scene.add(new assets.prefabs.wasp([15 + delay + i, spread * -2 - 1]));
    }
  }
}

function dragonflyWave(scene, count, phase) {
  const range = 20 / count;
  let top = -10;
  while (count-- > 0) {
    const dragonfly = new assets.prefabs.dragonfly([20, top + Math.random() * range]);
    top += range;
    dragonfly.lingerTime = 3000 * phase;
    scene.add(dragonfly);
  }
}

export default [
  [
    function(scene) {
      backMosquitos(scene);
      return 1;
    },
    function(scene) {
      backMosquitos(scene);
      sideMosquitos(scene, 1);
      return 2;
    },
    function(scene) {
      backMosquitos(scene);
      sideMosquitos(scene, 2);
      return 3;
    },
    function(scene) {
      backMosquitos(scene);
      sideMosquitos(scene, 3);
      return 4;
    },
    function(scene) {
      backMosquitos(scene);
      sideMosquitos(scene, 4);
      return 5;
    },
    function(scene) {
      sideMosquitos(scene, 1);
      scene.add(new assets.prefabs.mosquitoBoss([30, 0]));
      GameManager.engine.enqueue(5000, () => sideMosquitos(scene, 1));
      GameManager.enemiesRemaining = 4;
      return 4;
    }
  ],
  [
    function(scene) {
      sideMosquitos(scene, 2, false, 1);
      backWasps(scene, 3, 5);
      return 2;
    },
    function(scene) {
      backMosquitos(scene);
      sideMosquitos(scene, 2, false, 1);
      backWasps(scene, 3, 3);
      backWasps(scene, 4, 10, 3);
      return 3;
    },
    function(scene) {
      sideMosquitos(scene, 2, false, 1);
      sideMosquitos(scene, 2, false, -20);
      backWasps(scene, 3, 3);
      backWasps(scene, 4, 10, 3);
      backWasps(scene, 4, 20, 7);
      return 3;
    },
    function(scene) {
      backMosquitos(scene);
      sideMosquitos(scene, 1, false, 1);
      sideMosquitos(scene, 2, false, -20);
      backWasps(scene, 5, 3);
      backWasps(scene, 4, 10, 5);
      backWasps(scene, 4, 20);
      return 3;
    },
    function(scene) {
      backMosquitos(scene);
      scene.add(new assets.prefabs.waspBoss([30, 0]));
      return 4;
    },
  ],
  [
    function(scene) {
      backMosquitos(scene);
      dragonflyWave(scene, 1, 1);
      dragonflyWave(scene, 2, 2);
      return 2;
    },
    function(scene) {
      backMosquitos(scene);
      backWasps(scene, 1, 10);
      backWasps(scene, 1, 30);
      dragonflyWave(scene, 1, 0);
      dragonflyWave(scene, 2, 1);
      dragonflyWave(scene, 3, 2);
      return 3;
    },
    function(scene) {
      backMosquitos(scene);
      sideMosquitos(scene, 2, false, -40);
      backWasps(scene, 1, 10);
      backWasps(scene, 2, 30);
      dragonflyWave(scene, 2, 0);
      dragonflyWave(scene, 2, 1);
      dragonflyWave(scene, 2, 2.5);
      dragonflyWave(scene, 2, 4);
      return 4;
    },
    function(scene) {
      backMosquitos(scene);
      sideMosquitos(scene, 2);
      scene.add(new assets.prefabs.dragonflyBoss([30, 0]));
      return 5;
    },
  ],
  [
    function(scene) {
      const boss = new assets.prefabs.monstrosity([8, 0]);
      scene.add(boss);
      boss.immune = true;

      scene.add(boss.mosquito = new assets.prefabs.mosquitoBoss([5, -20]));
      boss.mosquito.maxHealth = 1;
      boss.mosquito.state = 'merge';
      boss.mosquito.immune = true;

      scene.add(boss.wasp = new assets.prefabs.waspBoss([5, 20]));
      boss.mosquito.maxHealth = 1;
      boss.wasp.state = 'merge';
      boss.wasp.immune = true;

      scene.add(boss.dragonfly = new assets.prefabs.dragonflyBoss([28, 0]));
      boss.mosquito.maxHealth = 1;
      boss.dragonfly.state = 'merge';
      boss.dragonfly.immune = true;
      return 5;
    },
  ],
];
