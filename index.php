<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, user-zoom=fixed" />
<style>
* {
  box-sizing: border-box;
  user-select: none;
  -moz-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
}
html, body {
  font-family: sans-serif;
}
#container {
  position: absolute;
}
#container > div {
  border: 1px solid black;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
#container > #statusBar {
  border: none;
  height: 5vh;
}
@media screen and (min-aspect-ratio: 1/1) {
  #container {
    top: 0;
    left: calc(50% - 66.67vh);
    width: 133.33vh;
    height: 100vh;
  }
  .splashText {
    font-size: 4vh;
  }
  .popover {
    line-height: 50vh;
  }
}
@media screen and (max-aspect-ratio: 1/1) {
  #container {
    top: calc(50% - 37.5vw);
    left: 0;
    width: 100vw;
    height: 75vw;
  }
  .splashText {
    font-size: 3vw;
  }
  .popover {
    line-height: 37.5vw;
  }
}
#splash, #helppage {
  background: rgba(255, 255, 255, .5);
  display: none;
  text-align: center;
}
h1 {
  font-weight: bold;
  font-size: 14vmin;
  margin: 5vmin 0 2vmin;
  font-style: italic;
  text-align: center;
}
.splashText {
  display: inline-block;
  width: 95%;
  border: 1px solid black;
  background: rgba(128, 192, 96, .7);
  padding: 2vmin;
  text-align: justify;
}
#helppage h1 {
  font-style: normal;
  font-size:  12vmin;
}
#helppage p {
  margin: .5vh 0 0 0;
}
#helppage img {
  height: 4vh;
  vertical-align: bottom;
}
#loading .animate {
  position: absolute;
  top: calc(50% - 12vmin);
  width: 100%;
  left: 0;
  text-align: center;
  font-size: 20vmin;
  font-weight: bold;
  animation: blink .5s infinite;
}
#progress {
  position: absolute;
  top: calc(50% + 8vmin);
  width: 100%;
  left: 0;
  text-align: center;
  font-size: 5vmin;
}
@keyframes blink {
  0% { color: black; }
  50% { color: transparent; }
}
#camera canvas {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  /*
  image-rendering: pixelated -moz-crisp-edges;
  -webkit-optimize-contrast: pixelated;
  */
}
#camera canvas:first-of-type {
  background-image: url('assets/paper.png');
  background-size: cover;
}
#pauseContainer {
  position: fixed;
  left: 40%;
  width: 20%;
  overflow: visible;
  bottom: 2vmin;
  text-align: center;
}
#dpad {
  position: fixed;
  background-color: #808080;
  left: 2vmin;
  bottom: 2vmin;
  width: 31vmin;
  height: 31vmin;
  border: 1px solid black;
  border-radius: 7vmin;
}
.dpad div {
  width: 33.5%;
  height: 33.5%;
  line-height: initial;
  text-align: center;
  border: 1px solid black;
  background-color: white;
  color: black;
  margin: -1px;
  box-sizing: content-box;
  border-radius: 5px;
}
.dpad span {
  position: absolute;
  top: calc(50% - 0.5em);
  left: calc(50% - 0.5em);
  height: 1em;
  width: 1em;
  text-align: center;
  display: inline-block;
}
.dpad div.active {
  background-color: black;
  color: white;
}
.dpad .up {
  position: absolute;
  top: 0;
  left: 33.25%;
}
.dpad .left {
  position: absolute;
  left: 0;
  top: 33.25%;
}
.dpad .right {
  position: absolute;
  right: 0;
  top: 33.25%;
}
.dpad .down {
  position: absolute;
  bottom: 0;
  left: 33.25%;
}
#buttons {
  position: fixed;
  right: 2vmin;
  bottom: 2vmin;
}
.button img {
  vertical-align: middle;
}
.buttons .button, .pause .button {
  text-align: center;
  border: 1px solid black;
  border-radius: 2vmin;
  margin: -1px;
  background-color: white;
  color: black;
}
.buttons .button {
  width: 30vmin;
  height: 30vmin;
  line-height: 30vmin;
}
.pause .button {
  display: inline-block;
  height: 7vmin;
  line-height: 7vmin;
  padding: 0 2vmin;
}
.buttons .button.active, .pause .button.active {
  background-color: black;
  color: white;
}
.bigbutton {
  display: inline-block;
  width: 40%;
  margin: 2vmin 2vw;
  padding: 2vmin 2vw;
  font-size: 6vmin;
  height: 12vmin;
  line-height: 8vmin;
  border: 2px solid black;
  border-radius: 5px;
  color: black;
  text-decoration: none;
}
.popover .bigbutton {
  width: 80%;
}
.bigbutton.start {
  background: linear-gradient(rgb(128, 255, 128), rgb(0, 128, 0));
}
.bigbutton.easy {
  background: linear-gradient(rgb(128, 255, 255), rgb(0, 128, 0));
}
.bigbutton.help {
  background: linear-gradient(rgb(255, 255, 128), rgb(128, 128, 0));
}
.bigbutton.start:active {
  background: linear-gradient(rgb(32, 128, 32), rgb(100, 192, 100));
}
.bigbutton.easy:active {
  background: linear-gradient(rgb(32, 128, 128), rgb(100, 192, 100));
}
.bigbutton.help:active {
  background: linear-gradient(rgb(128, 128, 32), rgb(220, 220, 0));
}
#helppage td, #helppage th {
  text-align: center;
  padding: .1vmin;
}
#container > .popover {
  background: rgba(255, 255, 128, .9);
  text-align: center;
  display: none;
  position: absolute;
  width: 50%;
  height: 50%;
  border: 1px solid black;
  top: 25%;
  left: 25%;
}
.popover > div {
  display: inline-block;
  vertical-align: middle;
  line-height: initial;
  width: 100%;
}
.popoverContent h2 {
  animation: blink 2s infinite;
}
#gameOverWin.popover {
  height: 90%;
  width: 90%;
  top: 5%;
  left: 5%;
}
#gameOverWin.popover img {
  max-height: 50%;
  max-width: 60%;
}
#butterflyAnimation {
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
}
h2 {
  font-size: 6vmin;
  padding: 0;
  margin: 0 0 4vh;
  font-weight: bold;
}
#statusBar div {
  min-width: 30%;
  margin: 1vmin;
  border: 1px solid white;
  border-radius: 3px;
  font-size: 3.2vmin;
  padding: 1vmin;
  color: white;
  background: #0000a0;
  font-weight: bold;
}
#statusBar.onTitle div {
  display: none;
}
#statusBar.onTitle div.onTitle {
  display: block;
}
#statusBar span {
  font-weight: normal;
}
#statusBar div.left {
  float: left;
  text-align: left;
}
#statusBar div.right {
  float: right;
  text-align: right;
}
</style>
</head>
<body>
<div id='fullscreen'>
<div id='container'>
  <div id='camera'></div>
  <div id='loading'>
    <div class='animate'>Loading...</div>
    <div id='progress'>0%</div>
  </div>
  <div id='splash'>
    <h1>Lily vs. the Swarm</h1>
    <div class='splashText'>
      It's spring, and that means new life! But not all of that life is friendly.
      Keep your friend, the butterfly, safe from the bad bugs!<br/><br/>
      Join this fairy and her friend in a child's imaginary adventure.
    </div>
    <a id='startButton' class='bigbutton start'>Start Game</a>
    <a id='easyButton' class='bigbutton easy' style='display:none'>Easy Mode</a>
    <a id='helpButton' class='bigbutton help'>Help</a>
  </div>
  <div id='helppage'>
    <h1>How to Play</h1>
    <div class='splashText'>
      <table width='100%'>
        <tr><th align='center' width='20%'>Action</th><th align='center' width='30%'>Keyboard</th><th align='center' width='50%'>Touchscreen</th></tr>
        <tr><td>Move</td><td>Arrow Keys/WASD</td><td>Virtual D-Pad (left side)</td></tr>
        <tr><td>Fire</td><td>Space Bar</td><td><img src='assets/fireball-24.png' />Button (right side)</td></tr>
        <tr><td>Bomb</td><td>Shift</td><td><img src='assets/flower1a.png' />Button (right side)</td></tr>
        <tr><td>Pause</td><td>Escape</td><td>Pause Button (center)</td></tr>
      </table>
      <p>Protect the butterfly from the swarm of bad bugs!</p>
      <p>Defeated bugs drop energy. When your energy bar is full, a bomb becomes available.
      Bombs deal damage and heal you or the butterfly. You can carry up to 5 bombs at once.</b>
    </div>
    <a id='helpBackButton' class='bigbutton help'>Back</a>
  </div>
  <div id='pauseBox' class='popover'>
    <div>
      <h2>PAUSED</h2>
      <a id='resumeButton' class='bigbutton start'>Resume</a>
    </div>
  </div>
  <div id='gameOver' class='popover'>
    <div>
      <h2>GAME OVER</h2>
      Having trouble? Try Easy Mode!
      <a id='gameOverStartButton' class='bigbutton start'>Return to Title</a>
    </div>
  </div>
  <div id='gameOverWin' class='popover'>
    <div>
      <h2>Victory!</h2>
      <img id='winScreen' src='assets/winscreen.png' /><br/>
      <div id='butterflyAnimation' style='position:absolute'></div>
      You've saved your friend from the bad bugs!<br/>
      <i>Easy Mode unlocked!</i>
      <a id='gameOverStartButton2' class='bigbutton start'>Return to Title</a>
    </div>
  </div>
  <div id='statusBar' class='onTitle'>
    <div class='left onTitle'>
      High Score: <span id='highScore'>0</span>
    </div>
    <div class='left'>
      Score: <span id='score'>0</span>
    </div>
  </div>
</div>
<div id='dpad'></div>
<div id='buttons'></div>
<div id='pauseContainer'></div>
</div>
<?php
function loadScript($script, $module = false) {
  echo "<script " . ($module ? 'type="module" ' : '' ) . "src='$script.js?cb=" . filemtime("$script.js") . "'></script>\n";
}
if ($_REQUEST['dev']) {
  loadScript('main', true);
} else {
  loadScript('dist/main', false);
}
?>
</body>
</html>
