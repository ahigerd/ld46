dist: dist/main.js

distclean:
	rm -rf dist redist
	rm -f build/assetlist.js
	rm -f build/assetImports.js

build/node_modules/.bin/%:
	cd build && npm install

dist/main.js: build/node_modules/.bin/webpack FORCE
	cd build && npm run build

clean: build/node_modules/.bin/webpack FORCE
	cd build && npm run clean

lint: build/node_modules/.bin/eslint
	build/node_modules/.bin/eslint -c .eslintrc.js .

redist: dist FORCE
	mkdir -p redist/assets
	mkdir -p redist/dist
	cp index.php redist
	cp assets/* redist/assets/
	cp dist/* redist/dist/

FORCE:
