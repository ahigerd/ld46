import assets from './scripts/assets';
import GameManager from './scripts/GameManager';
import { Input } from './Engine';

window.assets = assets;
Input.normalize['w'] = 'ArrowUp';
Input.normalize['W'] = 'ArrowUp';
Input.normalize['s'] = 'ArrowDown';
Input.normalize['S'] = 'ArrowDown';
Input.normalize['a'] = 'ArrowLeft';
Input.normalize['A'] = 'ArrowLeft';
Input.normalize['d'] = 'ArrowRight';
Input.normalize['D'] = 'ArrowRight';

const progress = document.getElementById('progress');
assets.addEventListener('loadprogress', ({ detail: percent}) => progress.innerText = (percent * 100).toFixed(0) + '%');
assets.addEventListener('loadcomplete', () => GameManager.init(assets), { once: true });
